const 太郎 = {
    id:"1",
    name: {
        first: "太郎",
        last: "山田",
    }
};

const 瑞希 = {
    id:"2",
    name:{
        first: "瑞希",
        last: "竹下",
    }
};

const イチロー = {
    id:"3",
    name:{
        first: "一郎",
        last: "田中",
    }
};

const fullname = (person) => {
    return "[" + person.id + "]" + person.name.first + " " + person.name.last;
};

console.log(fullname(太郎));
console.log(fullname(瑞希));
console.log(fullname(イチロー));
